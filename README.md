# **All utilities are now moved to `tbld/binaries`**. This repository is now unmaintained and obsolete.**
## utilities for tumbledemerald

This repo originally held buildscripts, miscellaneous documentation, and Pro Tips (tm) from us and other members of the community.

If you made it here by mistake and want the game source code, see the `game` repo
For the current buildscripts and utilities, see `tbld/binaries`.
